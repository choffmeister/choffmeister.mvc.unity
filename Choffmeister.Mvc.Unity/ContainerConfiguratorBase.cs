﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Microsoft.Practices.Unity;

namespace Choffmeister.Mvc.Unity
{
    public class ContainerConfiguratorBase
    {
        protected static IUnityContainer _container;

        public static IUnityContainer Container
        {
            get { return _container; }
        }

        public static void RegisterAllControllers()
        {
            EnsureBootstraped();

            Assembly assembly = Assembly.GetCallingAssembly();

            foreach (Type controllerType in assembly.GetTypes().Where(n => typeof(IController).IsAssignableFrom(n)))
            {
                string registrationName;

                if (controllerType.Name.EndsWith("Controller"))
                {
                    registrationName = controllerType.Name.Substring(0, controllerType.Name.Length - 10);
                }
                else
                {
                    registrationName = controllerType.Name;
                }

                _container.RegisterType(typeof(IController), controllerType, registrationName);
            }
        }

        protected static void EnsureBootstraped()
        {
            if (_container == null)
            {
                throw new Exception("You must bootstrap the container first.");
            }
        }
    }
}
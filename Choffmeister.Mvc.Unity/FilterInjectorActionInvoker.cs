﻿using System.Web.Mvc;
using Microsoft.Practices.Unity;

namespace Choffmeister.Mvc.Unity
{
    /// <summary>
    /// Injects property dependencies into <see cref="System.Web.Mvc.IActionFilter" />.
    /// </summary>
    public class FilterInjectorActionInvoker : ControllerActionInvoker
    {
        private readonly IUnityContainer _container;

        public FilterInjectorActionInvoker(IUnityContainer container)
        {
            _container = container;
        }

        protected override FilterInfo GetFilters(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            var filters = base.GetFilters(controllerContext, actionDescriptor);

            foreach (var filter in filters.ActionFilters)
            {
                _container.BuildUp(filter.GetType(), filter);
            }

            foreach (var filter in filters.AuthorizationFilters)
            {
                _container.BuildUp(filter.GetType(), filter);
            }

            foreach (var filter in filters.ExceptionFilters)
            {
                _container.BuildUp(filter.GetType(), filter);
            }

            foreach (var filter in filters.ResultFilters)
            {
                _container.BuildUp(filter.GetType(), filter);
            }

            return filters;
        }
    }
}
﻿using System;
using System.Web;

namespace Choffmeister.Mvc.Unity
{
    public class RequestLifeTimeManager : Microsoft.Practices.Unity.LifetimeManager
    {
        private readonly string _key = string.Format("PerCallContextOrRequestLifeTimeManager_{0}", Guid.NewGuid());

        public override object GetValue()
        {
            if (HttpContext.Current != null && HttpContext.Current.Items.Contains(_key))
                return HttpContext.Current.Items[_key];
            else
                return null;
        }

        public override void RemoveValue()
        {
            if (HttpContext.Current != null)
                HttpContext.Current.Items.Remove(_key);
        }

        public override void SetValue(object newValue)
        {
            if (HttpContext.Current != null)
                HttpContext.Current.Items[_key] = newValue;
        }
    }
}
﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using Microsoft.Practices.Unity;

namespace Choffmeister.Mvc.Unity
{
    public class UnityControllerFactory : IControllerFactory
    {
        private readonly IUnityContainer _container;

        public UnityControllerFactory(IUnityContainer container)
        {
            _container = container;
        }

        public IController CreateController(RequestContext requestContext, string controllerName)
        {
            IController controller = null;

            if (_container.IsRegistered<IController>(controllerName))
            {
                controller = _container.Resolve<IController>(controllerName);
            }

            if (controller as Controller != null)
            {
                ((Controller)controller).ActionInvoker = new FilterInjectorActionInvoker(_container);
            }

            return controller;
        }

        public SessionStateBehavior GetControllerSessionBehavior(System.Web.Routing.RequestContext requestContext, string controllerName)
        {
            return SessionStateBehavior.Default;
        }

        public void ReleaseController(IController controller)
        {
            IDisposable disposable = controller as IDisposable;

            if (disposable != null)
            {
                disposable.Dispose();
            }
        }
    }
}
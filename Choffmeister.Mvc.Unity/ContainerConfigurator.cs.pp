﻿using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Choffmeister.Mvc.Unity;

namespace $rootnamespace$
{
    public class ContainerConfigurator : ContainerConfiguratorBase
    {
        public static void Bootstrap()
        {
            _container = new UnityContainer();
            _container
                .RegisterType<IControllerFactory, UnityControllerFactory>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(_container));
        }
    }
}
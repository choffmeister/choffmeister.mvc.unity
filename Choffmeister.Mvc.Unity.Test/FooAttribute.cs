﻿using System;
using System.Web.Mvc;
using Microsoft.Practices.Unity;

namespace Choffmeister.Mvc.Unity.Test
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public sealed class FooAttribute : FilterAttribute, IActionFilter
    {
        private readonly string positionalString;

        public string PositionalString
        {
            get { return positionalString; }
        }

        public int NamedInt { get; set; }

        [Dependency]
        public IMyService MyService { get; set; }

        public FooAttribute(string positionalString)
        {
            this.positionalString = positionalString;
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }
    }
}
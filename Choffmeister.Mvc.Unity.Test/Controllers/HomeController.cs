﻿using System.Web.Mvc;

namespace Choffmeister.Mvc.Unity.Test.Controllers
{
    public class HomeController : Controller
    {
        [Foo("Bar", NamedInt = 23)]
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}